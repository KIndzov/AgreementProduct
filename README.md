Starting the program will create a test txt file in the /files/ directory and output the path in the console.(There is also an already created file)

The program has three endpoints

1: /api/save
    Given a JSON file of type Appointment will createa a txt file in /files/ directory and output path to it

2: /api/get
    Given a Json file with one field "path" with value the path given from the /api/save endpoint will create and return an Appointment object

3: /api/getUrl
    Given a parameter with name path and value(URL encoded) the path given from the /api/save endpoint will create and return an Appointment object
